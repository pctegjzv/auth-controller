// https://jenkins.io/doc/book/pipeline/syntax/
@Library('alauda-cicd@stateful') _

// global variables for pipeline
def GIT_BRANCH
def GIT_COMMIT
// image can be used for promoting...
def IMAGE
def CURRENT_VERSION
def code_data
def DEBUG = false
def deployment
def RELEASE_VERSION
def CHART_VERSION
def RELEASE_BUILD
def TEST_IMAGE
pipeline {
    // 运行node条件
    // 为了扩容jenkins的功能一般情况会分开一些功能到不同的node上面
    // 这样每个node作用比较清晰，并可以并行处理更多的任务量
    agent {
        label 'all'
    }

    // (optional) 流水线全局设置
    options {
        // 保留多少流水线记录（建议不放在jenkinsfile里面）
        buildDiscarder(logRotator(numToKeepStr: '10'))

        // 不允许并行执行
        disableConcurrentBuilds()
    }
    //(optional) 环境变量
    environment {
        FOLDER = '$GOPATH/src/alauda.io/auth-controller'

        // for building an scanning
        REPOSITORY = "auth-controller"
        OWNER = "mathildetech"
        // sonar feedback user
        // needs to change together with the credentialsID
        BITBUCKET_FEEDBACK_ACCOUNT = "alaudabot"
        SONARQUBE_BITBUCKET_CREDENTIALS = "alaudabot"
        NAMESPACE = "alauda-system"
        DEPLOYMENT_NAME = "auth-controller"
        CONTAINER = "manager"
        DINGDING_BOT = "auth-bot"
        TAG_CREDENTIALS = "alaudabot-bitbucket"
        PATH = "${PATH}:/usr/local/kubebuilder/bin"
    }
    // stages
    stages {
        stage('Checkout') {
            steps {
                script {
                    // checkout code
                    def scmVars = checkout scm
                    // extract git information
                    env.GIT_COMMIT = scmVars.GIT_COMMIT
                    env.GIT_BRANCH = scmVars.GIT_BRANCH
                    GIT_COMMIT = "${scmVars.GIT_COMMIT}"
                    GIT_BRANCH = "${scmVars.GIT_BRANCH}"
                    RELEASE_VERSION = readFile('.version').trim()
                    RELEASE_BUILD = "${RELEASE_VERSION}.${env.BUILD_NUMBER}"
                    if (GIT_BRANCH != "master") {
                        def branch = GIT_BRANCH.replace("/","-").replace("_","-")
                        RELEASE_BUILD = "${RELEASE_VERSION}.${branch}.${env.BUILD_NUMBER}".toLowerCase()
                    }
                    // moving project code for the specified folder
                    sh """
                        rm -rf ${FOLDER}
                        mkdir -p ${FOLDER}
                        cp -R . ${FOLDER}
                        cp -R .git ${FOLDER}/.git
                    """
                    // installing golang coverage and report tools
                    sh """
                        go get -u github.com/alauda/gitversion
                    """
                    // installing kubebuilder
                    sh '''
                        version=1.0.0 # latest stable version
                        arch=amd64

                        # download the release
                        curl -L -O https://github.com/kubernetes-sigs/kubebuilder/releases/download/v${version}/kubebuilder_${version}_linux_${arch}.tar.gz

                        # extract the archive
                        tar -zxvf kubebuilder_${version}_linux_${arch}.tar.gz
                        mv kubebuilder_${version}_linux_${arch} /usr/local/kubebuilder
                        # install etcd
                        go get go.etcd.io/etcd
                        mv $GOPATH/bin/etcd /usr/local/kubebuilder/bin
                        chmod +x /usr/local/kubebuilder/bin/*
                    '''

                    if (GIT_BRANCH == "master") {
                        sh "gitversion patch ${RELEASE_VERSION} > patch"
                        RELEASE_BUILD = readFile("patch").trim()
                    }
                    echo "release ${RELEASE_VERSION} - release build ${RELEASE_BUILD}"
                }
            }
        }
        stage('CI'){
            failFast true
            parallel {
                stage('Build') {
                    steps {
                        script {
                            sh """
                                cd ${FOLDER}
                                CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -a -o ./bin/auth-controller alauda.io/auth-controller/cmd/manager
                                upx ./bin/auth-controller || true
                                cp config/images/Dockerfile ./bin/
                            """

                            // currently is building code inside the container
                            IMAGE = deploy.dockerBuild(
                                "${FOLDER}/bin/Dockerfile", //Dockerfile
                                "${FOLDER}/bin", // build context
                                "index.alauda.cn/alaudak8s/auth-controller", // repo address
                                "${RELEASE_BUILD}", // tag
                                "alaudak8s", // credentials for pushing
                            ).setArg("commit_id", "${GIT_COMMIT}").setArg("app_version", "${RELEASE_BUILD}")
                            // start and push
                            IMAGE.start().push().push(GIT_COMMIT)
                        }

                    }
                }
                stage('Code Scan') {
                    steps {
                        script {
                            // generate reports
                            sh "echo 'sonar.projectVersion=${RELEASE_BUILD}' >> ${FOLDER}/sonar-project.properties"
                            sh """
                                cd ${FOLDER}
                                make test
                            """
                            
                            deploy.scan(
                                REPOSITORY,
                                GIT_BRANCH,
                                SONARQUBE_BITBUCKET_CREDENTIALS,
                                FOLDER,
                                DEBUG,
                                OWNER,
                                BITBUCKET_FEEDBACK_ACCOUNT
                            ).start()
                        }
                    }
                }
            }
        }
        // after build it should start deploying
        stage('Deploying') {
            steps {
                script {
                    // setup kubectl
                    if (GIT_BRANCH == "master") {
                        // master is already merged
                        deploy.setupStaging()

                    } else {
                        // pull-requests
                        deploy.setupInt()
                    }
                    // saving current state
                    CURRENT_VERSION = deploy.getDeployment(NAMESPACE, DEPLOYMENT_NAME, "statefulset")

                    deployment = deploy.getYaml("./config/manager/manager.yaml").getCurrentState()
                    deployment.setImage(IMAGE.getImage(), "auth-controller-manager")
                    // start deploying
                    deployment.apply()
                }
            }
        }
        stage('Promoting') {
            // limit this stage to master only
            when {
                expression {
                    GIT_BRANCH == "master"
                }
            }
            steps {
                script {
                    // setup kubectl
                    deploy.setupProd()

                    // promote to release
                    IMAGE.push("release")

                    deployment.apply()

                    // adding tag to the current commit
                    withCredentials([usernamePassword(credentialsId: TAG_CREDENTIALS, passwordVariable: 'GIT_PASSWORD', usernameVariable: 'GIT_USERNAME')]) {
                        sh "git tag -l | xargs git tag -d" // clean local tags
                        sh """
                            git config --global user.email "alaudabot@alauda.io"
                            git config --global user.name "Alauda Bot"
                        """
                        def repo = "https://${GIT_USERNAME}:${GIT_PASSWORD}@bitbucket.org/${OWNER}/${REPOSITORY}.git"
                        sh "git fetch --tags ${repo}" // retrieve all tags
                        sh("git tag -a ${RELEASE_BUILD} -m 'auto add release tag by jenkins'")
                        sh("git push ${repo} --tags")
                    }
                    build job: '../../charts-pipeline', parameters: [
                    [$class: 'StringParameterValue', name: 'CHART', value: 'portal'],
                    [$class: 'StringParameterValue', name: 'VERSION', value: ''],
                    [$class: 'StringParameterValue', name: 'COMPONENT', value: 'auth-controller'],
                    [$class: 'StringParameterValue', name: 'IMAGE_TAG', value: RELEASE_BUILD],
                    [$class: 'BooleanParameterValue', name: 'DEBUG', value: false],
                    [$class: 'StringParameterValue', name: 'ENV', value: ''],
                    ], wait: false
                }
            }
        }

    }

    // (optional)
    // happens at the end of the pipeline
    post {
        // 成功
        success {
            echo "Horay!"
            script {
                if (GIT_BRANCH == "master") {
                    deploy.notificationSuccess(DEPLOYMENT_NAME, DINGDING_BOT, "上线啦！", RELEASE_BUILD)
                } else {
                    deploy.notificationSuccess(DEPLOYMENT_NAME, DINGDING_BOT, "流水线完成了", RELEASE_BUILD)
                }
            }
        }
        // 失败
        failure {
            echo "damn!"
            script {
                if (deployment != null) {
                    if (GIT_BRANCH == "master") {
                        deploy.setupStaging()
                    } else {
                        deploy.setupInt()
                    }
                    deployment.rollbackConfig().apply()
                }
                deploy.notificationFailed(DEPLOYMENT_NAME, DINGDING_BOT, "流水线失败了", RELEASE_BUILD)
            }
        }
    }
}
