package clusterrole

const SUPER_ADMIN = "alauda_super_admin"
const ADMIN = "alauda_admin"
const UNTRUSTED_ADMIN = "alauda_untrusted_admin"
const PLATFORM_AUDITOR = "alauda_platform_auditor"
const PROJECT_ADMIN = "alauda_project_admin"
const PROJECT_AUDITOR = "alauda_project_auditor"
const NAMESPACE_ADMIN = "alauda_namespace_admin"
const NAMESPACE_DEVELOPER = "alauda_namespace_developer"
const NAMESPACE_AUDITOR = "alauda_namespace_auditor"

const EXT_PROJECT_ADMIN = "ext_alauda_project_admin"
const EXT_PROJECT_AUDITOR = "ext_alauda_project_auditor"
const EXT_NAMESPACE_DEVELOPER = "ext_alauda_namespace_developer"
const EXT_NAMESPACE_ADMIN = "ext_alauda_namespace_admin"
const EXT_NAMESPACE_AUDITOR = "ext_alauda_namespace_auditor"
