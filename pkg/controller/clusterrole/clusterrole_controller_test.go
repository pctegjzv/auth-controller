/*
Copyright 2018 Alauda.io.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package clusterrole

import (
	"alauda.io/auth-controller/pkg/schema/clusterrole"
	"testing"
	"time"

	"github.com/onsi/gomega"
	"golang.org/x/net/context"
	testv1 "k8s.io/api/rbac/v1"
	apierrors "k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/types"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/manager"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
)

var c client.Client

var roleName = "foo-clusterrole"
var expectedRequest = reconcile.Request{NamespacedName: types.NamespacedName{Name: roleName}}
var depKey = types.NamespacedName{Name: roleName}

// clusterrole schema
var schemaClusterrole = &testv1.ClusterRole{
	TypeMeta: metav1.TypeMeta{
		Kind:       "ClusterRole",
		APIVersion: "rbac.authorization.k8s.io/v1",
	},
	ObjectMeta: metav1.ObjectMeta{
		Name: roleName,
	},
	Rules: []testv1.PolicyRule{
		{
			APIGroups: []string{
				"*",
			},
			Resources: []string{
				"*",
			},
			Verbs: []string{
				"*",
			},
		},
	},
}

const timeout = time.Second * 5

func TestReconcile(t *testing.T) {
	// init default role map
	clusterrole.DefaultClusterRoles[roleName] = schemaClusterrole

	g := gomega.NewGomegaWithT(t)

	// Setup the Manager and Controller.  Wrap the Controller Reconcile function so it writes each request to a
	// channel when it is finished.
	mgr, err := manager.New(cfg, manager.Options{})
	g.Expect(err).NotTo(gomega.HaveOccurred())
	c = mgr.GetClient()

	recFn, requests := SetupTestReconcile(newReconciler(mgr))
	g.Expect(add(mgr, recFn)).NotTo(gomega.HaveOccurred())
	defer close(StartTestManager(mgr, g))

	// Create the Role object and expect the Reconcile and Deployment to be created
	instance := schemaClusterrole.DeepCopy()
	err = c.Create(context.TODO(), instance)
	if apierrors.IsInvalid(err) {
		t.Logf("failed to create object, got an invalid object error: %v", err)
		return
	}
	g.Expect(err).NotTo(gomega.HaveOccurred())
	defer c.Delete(context.TODO(), instance)
	g.Eventually(requests, timeout).Should(gomega.Receive(gomega.Equal(expectedRequest)))

	// Delete the Clusterrole and expect Reconcile to be called for Clusterrole deletion
	g.Expect(c.Delete(context.TODO(), instance)).NotTo(gomega.HaveOccurred())
	g.Eventually(func() error { return c.Get(context.TODO(), depKey, instance) }, timeout).
		Should(gomega.Succeed())

	// Update the Clusterrole and expect Reconcile to be called
	instanceNew := schemaClusterrole.DeepCopy()
	instanceNew.Rules[0].Verbs[0] = "get"
	g.Expect(c.Update(context.TODO(), instanceNew)).NotTo(gomega.HaveOccurred())

	// send request
	g.Eventually(requests, timeout).Should(gomega.Receive(gomega.Equal(expectedRequest)))
	time.Sleep(timeout)

	// check reconcile
	g.Eventually(func() error { return c.Get(context.TODO(), depKey, instance) }, timeout).
		Should(gomega.Succeed())
	g.Eventually(instance.Rules).Should(gomega.Equal(schemaClusterrole.Rules))

}
