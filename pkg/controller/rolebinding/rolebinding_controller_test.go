/*
Copyright 2018 Alauda.io.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package rolebinding

import (
	"alauda.io/auth-controller/pkg/constant/label"
	"alauda.io/auth-controller/pkg/schema/clusterrolebinding"
	"testing"
	"time"

	// testv1 "alauda.io/auth-controller/pkg/apis/test/v1"
	"github.com/onsi/gomega"
	"golang.org/x/net/context"
	testv1 "k8s.io/api/rbac/v1"
	apierrors "k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/types"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/manager"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
)

var c client.Client

var namespace = "default"
var clusterroleName = "foo-clusterrole"
var userName = "chhu"
var extClusterroleName = "ext-foo-clusterrole"
var rolebindingName = clusterroleName + ":" + userName
var clusterrolebindingName = clusterroleName + "-" + userName + "-clusterrolebinding"

var expectedRequest = reconcile.Request{NamespacedName: types.NamespacedName{Name: rolebindingName, Namespace: namespace}}
var depKey = types.NamespacedName{Name: clusterrolebindingName}

// rolebinding schema
var SchemaRolebinding = &testv1.RoleBinding{
	TypeMeta: metav1.TypeMeta{
		Kind:       "RoleBinding",
		APIVersion: "rbac.authorization.k8s.io/v1",
	},
	ObjectMeta: metav1.ObjectMeta{
		Name: rolebindingName,
		Labels: map[string]string{
			label.SYSTEM_ROLEBINDING: "true",
		},
		Namespace: namespace,
	},
	RoleRef: testv1.RoleRef{
		APIGroup: "rbac.authorization.k8s.io",
		Kind:     "ClusterRole",
		Name:     clusterroleName,
	},
	Subjects: []testv1.Subject{
		{
			Kind:     "User",
			Name:     userName,
			APIGroup: "rbac.authorization.k8s.io",
		},
	},
}

// clusterrolebinding schema
var SchemaClusterRolebinding = &testv1.ClusterRoleBinding{
	TypeMeta: metav1.TypeMeta{
		Kind:       "ClusterRoleBinding",
		APIVersion: "rbac.authorization.k8s.io/v1",
	},
	ObjectMeta: metav1.ObjectMeta{
		// to be replaced
		Name: "<clusterrolebindingname>",
		Labels: map[string]string{
			label.SYSTEM_CLUSTERROLEBINDING: "true",
		},
	},
	RoleRef: testv1.RoleRef{
		APIGroup: "rbac.authorization.k8s.io",
		Kind:     "ClusterRole",
		Name:     extClusterroleName,
	},
	Subjects: []testv1.Subject{
		{
			Kind: "User",
			// to be replaced
			Name:     "<username>",
			APIGroup: "rbac.authorization.k8s.io",
		},
	},
}

const timeout = time.Second * 5

func TestReconcile(t *testing.T) {
	// init clusterrolebinding map
	clusterrolebinding.ClusterRoleBindingMap[clusterroleName] = SchemaClusterRolebinding

	// Setup the Manager and Controller.  Wrap the Controller Reconcile function so it writes each request to a
	// channel when it is finished.
	mgr, err := manager.New(cfg, manager.Options{})
	g := gomega.NewGomegaWithT(t)
	g.Expect(err).NotTo(gomega.HaveOccurred())
	c = mgr.GetClient()

	recFn, requests := SetupTestReconcile(newReconciler(mgr))
	g.Expect(add(mgr, recFn)).NotTo(gomega.HaveOccurred())
	defer close(StartTestManager(mgr, g))

	// Create the rolebinding object and expect the Reconcile and clusterrolebinding to be created
	instance := SchemaRolebinding.DeepCopy()
	err = c.Create(context.TODO(), instance)
	if apierrors.IsInvalid(err) {
		t.Logf("failed to create object, got an invalid object error: %v", err)
		return
	}
	g.Expect(err).NotTo(gomega.HaveOccurred())
	g.Eventually(requests, timeout).Should(gomega.Receive(gomega.Equal(expectedRequest)))

	// should get the clusterrolebinding
	crb := &testv1.ClusterRoleBinding{}
	g.Eventually(func() error { return c.Get(context.TODO(), depKey, crb) }, timeout).
		Should(gomega.Succeed())

	// delete the rolebinding
	g.Expect(c.Delete(context.TODO(), instance)).NotTo(gomega.HaveOccurred())

	g.Eventually(
		func() metav1.StatusReason {
			err := c.Get(context.TODO(), types.NamespacedName{Name: rolebindingName, Namespace: namespace}, instance)
			return apierrors.ReasonForError(err)
		}, timeout).
		Should(gomega.Equal(metav1.StatusReasonNotFound))

}
