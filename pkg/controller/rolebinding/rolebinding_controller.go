/*
Copyright 2018 Alauda.io.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package rolebinding

import (
	"alauda.io/auth-controller/pkg/constant/label"
	"alauda.io/auth-controller/pkg/schema/clusterrolebinding"
	"context"
	testv1 "k8s.io/api/rbac/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/apimachinery/pkg/util/uuid"
	"log"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	"sigs.k8s.io/controller-runtime/pkg/manager"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
	"sigs.k8s.io/controller-runtime/pkg/source"
)

/**
* USER ACTION REQUIRED: This is a scaffold file intended for the user to modify with their own Controller
* business logic.  Delete these comments after modifying this file.*
 */

// Add creates a new Role Controller and adds it to the Manager with default RBAC. The Manager will set fields on the Controller
// and Start it when the Manager is Started.
// USER ACTION REQUIRED: update cmd/manager/main.go to call this test.Add(mgr) to install this Controller
func Add(mgr manager.Manager) error {
	return add(mgr, newReconciler(mgr))
}

// newReconciler returns a new reconcile.Reconciler
func newReconciler(mgr manager.Manager) reconcile.Reconciler {
	return &ReconcileRoleBinding{Client: mgr.GetClient(), scheme: mgr.GetScheme()}
}

// add adds a new Controller to mgr with r as the reconcile.Reconciler
func add(mgr manager.Manager, r reconcile.Reconciler) error {
	// Create a new controller
	c, err := controller.New("rolebinding-controller", mgr, controller.Options{Reconciler: r})
	if err != nil {
		return err
	}

	// Watch for changes to RoleBinding
	err = c.Watch(&source.Kind{Type: &testv1.RoleBinding{}}, &handler.EnqueueRequestForObject{})
	if err != nil {
		return err
	}

	return nil
}

var _ reconcile.Reconciler = &ReconcileRoleBinding{}

// ReconcileRole reconciles a Role object
type ReconcileRoleBinding struct {
	client.Client
	scheme *runtime.Scheme
}

// Reconcile reads that state of the cluster for a Role object and makes changes based on the state read
// and what is in the Role.Spec
// a Deployment as an example
// Automatically generate RBAC rules to allow the Controller to read and write Deployments
// +kubebuilder:rbac:groups=apps,resources=deployments,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=test.alauda.io,resources=rolebindings,verbs=get;list;watch;create;update;patch;delete
func (r *ReconcileRoleBinding) Reconcile(request reconcile.Request) (reconcile.Result, error) {
	requestUuid := string(uuid.NewUUID())
	log.Printf("%s ReconcileRoleBinding request name: %s\n", requestUuid, request.NamespacedName)

	// Fetch the RoleBinding instance
	instance := &testv1.RoleBinding{}
	err := r.Get(context.TODO(), request.NamespacedName, instance)
	if err != nil {
		if errors.IsNotFound(err) {
			log.Printf("%s ReconcileRoleBinding not found name: %s\n", requestUuid, request.NamespacedName)
			// Object not found, return.
			return reconcile.Result{}, nil
		}
		// Error reading the object - requeue the request.
		log.Printf("%s ReconcileRoleBinding [error] err at Get: %v\n", requestUuid, err)
		return reconcile.Result{}, err
	}

	//log.Printf("ReconcileRoleBinding find instance name: %s\n", instance.Name)

	// find system RoleBinding
	systemRoleBindingExist := false
	if len(instance.Labels) > 0 {
		val, ok := instance.Labels[label.SYSTEM_ROLEBINDING]
		systemRoleBindingExist = ok && val == "true"
	}

	if systemRoleBindingExist {
		log.Printf("%s ReconcileRoleBinding [event] find system RoleBinding: %s\n", requestUuid, instance.Name)

		userName := instance.Subjects[0].Name
		roleName := instance.RoleRef.Name
		clusterRoleBindingName := roleName + "-" + userName + "-clusterrolebinding"

		log.Printf("%s ReconcileRoleBinding [event] userName/roleName: %s/%s\n", requestUuid, userName, roleName)

		object := &testv1.ClusterRoleBinding{}
		if clusterRoleBindingSchema, ok := clusterrolebinding.ClusterRoleBindingMap[roleName]; ok {
			log.Printf("%s ReconcileRoleBinding [event] clusterRoleBindingSchema: %v\n", requestUuid, clusterRoleBindingSchema)
			object = clusterRoleBindingSchema.DeepCopy()
			object.Name = clusterRoleBindingName
			object.Subjects[0].Name = userName
		} else {
			errInfo := requestUuid + " roleName [" + roleName + "] is not in the ClusterRoleBindingMap"
			return reconcile.Result{}, errors.NewBadRequest(errInfo)
		}

		// Check if the clusterrolebinding already exists
		found := &testv1.ClusterRoleBinding{}
		err = r.Get(context.TODO(), types.NamespacedName{Name: clusterRoleBindingName}, found)
		if err != nil && errors.IsNotFound(err) {

		}

		if err != nil {
			if errors.IsNotFound(err) {
				// SetControllerReference to the object
				if err := controllerutil.SetControllerReference(instance, object, r.scheme); err != nil {
					return reconcile.Result{}, err
				}
				log.Printf("%s ReconcileRoleBinding [event] creating %s\n", requestUuid, object.Name)
				err = r.Create(context.TODO(), object)
				if err != nil {
					return reconcile.Result{}, err
				}
			}
			return reconcile.Result{}, err
		}

	}

	return reconcile.Result{}, nil
}
