
# Image URL to use all building/pushing image targets
IMG ?= controller:latest
TAG=dev-$(shell cat .version)-$(shell git config --get user.email | sed -e "s/@/-/")
IMAGE=index.alauda.cn/alaudak8s/auth-controller

all: test manager

# Run tests
test: fmt vet
	go test ./pkg/... ./cmd/... -coverprofile cover.out
	go test ./pkg/... ./cmd/... -cover -json > test.json

# Build manager binary
manager: generate fmt vet
	go build -o bin/manager alauda.io/auth-controller/cmd/manager

# Run against the configured Kubernetes cluster in ~/.kube/config
run: generate fmt vet
	go run ./cmd/manager/main.go

# Install CRDs into a cluster
install: manifests
	kubectl apply -f config/crds

# Deploy controller in the configured Kubernetes cluster in ~/.kube/config
deploy: manifests
	kubectl apply -f config/crds
	kustomize build config/default | kubectl apply -f -

# Generate manifests e.g. CRD, RBAC etc.
manifests:
	go run vendor/sigs.k8s.io/controller-tools/cmd/controller-gen/main.go all

# Run go fmt against code
fmt:
	go fmt ./pkg/... ./cmd/...

# Run go vet against code
vet:
	go vet ./pkg/... ./cmd/...

# Generate code
# generate:
# 	go generate ./pkg/... ./cmd/...

build:
	CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -a -o ./bin/auth-controller alauda.io/auth-controller/cmd/manager
	upx ./bin/auth-controller || true

# Build the docker image
docker-build: test build
	cp config/images/Dockerfile ./bin/
	docker build -t ${IMAGE}:${TAG} -f ./bin/Dockerfile ./bin
	@echo "updating kustomize image patch file for manager resource"
	kubectl set image statefulset/auth-controller -n alauda-system manager=${IMAGE}:${TAG}

# Push the docker image
docker-push:
	docker push ${IMAGE}:${TAG}
